$ = jQuery.noConflict();

$(function(){  
  if ( !isMobile.any ) {
    $(window).scroll(function(){  
      if ($(window).scrollTop() > 140) {  
        $("header#masthead").addClass("sticky-menu-cart");
        $("#content").addClass("sticky-add-content-top-space");  
      } else {  
        $("header#masthead").removeClass("sticky-menu-cart");
        $("#content").removeClass("sticky-add-content-top-space");  
      }  
    });  
  }
});  