<?php

/**
 * Loads the StoreFront parent theme stylesheet.
 */
add_action( 'wp_enqueue_scripts', 'sf_child_theme_enqueue_styles' );
function sf_child_theme_enqueue_styles() {
	wp_enqueue_style( 'storefront-child-style', get_stylesheet_directory_uri() . '/style.css', array( 'storefront-style' ) );
	wp_enqueue_style( 'font-awesome', get_stylesheet_directory_uri() . '/assets/vendor/font-awesome/css/font-awesome.min.css' );
}

/**
 * Note: DO NOT! alter or remove the code above this text and only add your custom PHP functions below this text.
 */

// Remove WooThemes branding in footer.
add_filter( 'storefront_credit_link', false );

// Change the categories display on the homepage.
// add_filter( 'storefront_product_categories_args', 'custom_home_product_categories' );
// function custom_home_product_categories( $args ) {

//     $args['limit'] = 4;
//     $args['columns'] = 4;

//     return $args;
// }

// Changing the text on the add to cart button for External/Affiliate linked products
function gc_change_cart_button_text( $val, $product ) {
	return __( ( $product->button_text ) ? $product->button_text : 'Add to cart', 'woocommerce' );
}
add_filter( 'woocommerce_product_single_add_to_cart_text', 'gc_change_cart_button_text', 10, 2 );


// Hide SKU's from frontend single product pages.
// function gc_remove_product_page_skus( $enabled ) {
// 	if ( ! is_admin() && is_product() ) {
// 		return false;
// 	}

// 	return $enabled;
// }
// add_filter( 'wc_product_sku_enabled', 'gc_remove_product_page_skus' );


/**
 * Override Infinite Scroll script
 */
add_action( 'wp_enqueue_scripts', 'gctvshop_jscroll_init_override', 100 );
function gctvshop_jscroll_init_override() {
	wp_dequeue_script( 'jscroll-init' );
	wp_enqueue_script( 'custom-jscroll-init', get_stylesheet_directory_uri() . '/assets/js/jscroll-init.js', array( 'jscroll' ), null, true );
}

// Remove category titles from home page category section.
function gctvshop_rm_home_cat_titles() {
	if ( is_front_page() ) remove_action( 'woocommerce_shop_loop_subcategory_title', 'woocommerce_template_loop_category_title' );
}
add_action( 'wp', 'gctvshop_rm_home_cat_titles' );

// Remove Sales Flash from single product pages.
function gctvshop_rm_sales_flash_spp() {
	if ( is_product() ) return false;
}
add_filter( 'woocommerce_sale_flash', 'gctvshop_rm_sales_flash_spp' );

/**
 * Extra homepage sections.
 */
// Display Webcasts Products
if ( ! function_exists( 'gctvshop_webinars_products' ) ) {
	function gctvshop_webinars_products( $args ) {

		if ( is_woocommerce_activated() ) {

			$args = apply_filters( 'gctvshop_webinars_products_args', array(
				'limit' 			=> 4,
				'columns' 			=> 4,
				'orderby'			=> 'date',
				'order'				=> 'desc',
				'title'				=> __( 'Webinars', 'storefront' ),
				'category'			=> 'on-demand-webcasts',	// On-Demand Webcasts
				) );

			echo '<section class="storefront-product-section gctvshop-webinars-products">';

			do_action( 'gctvshop_homepage_before_webinars_products' );

			echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

			do_action( 'gctvshop_homepage_after_webinars_products_title' );

			echo storefront_do_shortcode( 'recent_products',
				array(
					'per_page' 	=> intval( $args['limit'] ),
					'columns'	=> intval( $args['columns'] ),
					'orderby'	=> esc_attr( $args['orderby'] ),
					'order'		=> esc_attr( $args['order'] ),
					'category'	=> esc_attr( $args['category'] ),
					) );

			do_action( 'gctvshop_homepage_after_webinars_products' );

			echo '</section>';

		}
	}
}

// Display Packages Products
if ( ! function_exists( 'gctvshop_packages_products' ) ) {
	function gctvshop_packages_products( $args ) {

		if ( is_woocommerce_activated() ) {

			$args = apply_filters( 'gctvshop_packages_products_args', array(
				'limit' 			=> 4,
				'columns' 			=> 4,
				'orderby'			=> 'date',
				'order'				=> 'desc',
				'title'				=> __( 'Packages', 'storefront' ),
				'category'			=> 'packages',	// Packages
				) );

			echo '<section class="storefront-product-section gctvshop-packages-products">';

			do_action( 'gctvshop_homepage_before_packages_products' );

			echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

			do_action( 'gctvshop_homepage_after_packages_products_title' );

			echo storefront_do_shortcode( 'recent_products',
				array(
					'per_page' 	=> intval( $args['limit'] ),
					'columns'	=> intval( $args['columns'] ),
					'orderby'	=> esc_attr( $args['orderby'] ),
					'order'		=> esc_attr( $args['order'] ),
					'category'	=> esc_attr( $args['category'] ),
					) );

			do_action( 'gctvshop_homepage_after_packages_products' );

			echo '</section>';

		}
	}
}

// Display eBooks Products
if ( ! function_exists( 'gctvshop_ebooks_products' ) ) {
	function gctvshop_ebooks_products( $args ) {

		if ( is_woocommerce_activated() ) {

			$args = apply_filters( 'gctvshop_ebooks_products_args', array(
				'limit' 			=> 4,
				'columns' 			=> 4,
				'orderby'			=> 'date',
				'order'				=> 'desc',
				'title'				=> __( 'eBooks', 'storefront' ),
				'category'			=> 'ebooks',	// eBooks
				) );

			echo '<section class="storefront-product-section gctvshop-ebooks-products">';

			do_action( 'gctvshop_homepage_before_ebooks_products' );

			echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

			do_action( 'gctvshop_homepage_after_ebooks_products_title' );

			echo storefront_do_shortcode( 'recent_products',
				array(
					'per_page' 	=> intval( $args['limit'] ),
					'columns'	=> intval( $args['columns'] ),
					'orderby'	=> esc_attr( $args['orderby'] ),
					'order'		=> esc_attr( $args['order'] ),
					'category'	=> esc_attr( $args['category'] ),
					) );

			do_action( 'gctvshop_homepage_after_ebooks_products' );

			echo '</section>';

		}
	}
}

// Add the sections to the homepage action hook of Storefront Theme.
add_action( 'homepage', 'gctvshop_webinars_products' );
add_action( 'homepage', 'gctvshop_packages_products' );
add_action( 'homepage', 'gctvshop_ebooks_products' );

// Hook AddThis after product information.
function gctvshop_show_addthis() {
	$customAddThis = array(
		'type' => 'custom',
    	'size' => '32', // size of the icons.  Either 16 or 32
    	'services' => 'facebook,twitter,google_plusone_share,linkedin,pinterest_share,buffer,email', // the services you want to always appear
    	// 'preferred' => '1', // the number of auto personalized services
    	// 'more' => false, // if you want to have a more button at the end
    	// 'counter' => 'bubble_style' // if you want a counter and the style of it
    	);
	do_action( 'addthis_widget', null, null, $customAddThis );
}
add_action( 'woocommerce_single_product_summary', 'gctvshop_show_addthis', 99 );

// Add Fake Stock Number.
add_action( 'woocommerce_before_add_to_cart_button', 'gctvshop_fake_stock_number', 10, 0 ); 
function gctvshop_fake_stock_number() {
	$fake_stock_number = types_render_field( 'custom-stock-number' ); ?>
	<?php if ( $fake_stock_number ): ?>
	<div class="on-stock-number">
		<span><?php _e( sprintf( 'Only %1$s left in stock', $fake_stock_number ), 'storefront' ) ?></span>
		<div class="clear"></div>
	</div>
<?php endif; ?>
<?php }

/**
 * Sticky Menu and Cart.
 */
add_action( 'wp_enqueue_scripts', 'gctvshop_sticky_menu_and_cart' );
function gctvshop_sticky_menu_and_cart() {
	if ( !is_checkout() ) {
		wp_enqueue_script( 'is-mobile', get_stylesheet_directory_uri() . '/assets/vendor/isMobile/isMobile.min.js', null, null, true );
		wp_enqueue_script( 'sticky-menu-and-cart', get_stylesheet_directory_uri() . '/assets/js/sticky-menu-and-cart.js', array( 'jquery', 'is-mobile' ), null, true );
	}
}


/*
 * Hide coupon field on cart page.
 */
add_filter( 'woocommerce_coupons_enabled', 'gctvshop_hide_coupon_field_on_cart' );
function gctvshop_hide_coupon_field_on_cart( $enabled ) {
	if ( is_cart() ) {
		$enabled = false;
	}
	return $enabled;
}

/*
 * Output the full image for the category thumbnails in the home page.
 */
add_filter( 'subcategory_archive_thumbnail_size', 'gctvshop_output_full_category_image' );
function gctvshop_output_full_category_image( $thumb_size ) {
	if ( is_front_page() ) return 'full';
	return $thumb_size;
}

/*
 * Custom Simplemodal login for cart page.
 */
add_filter( 'simplemodal_login_form', 'gctvshop_go_to_checkout_login_form' );
function gctvshop_go_to_checkout_login_form( $form ) {
	$users_can_register = get_option( 'users_can_register' ) ? true : false;
	$options = get_option( 'simplemodal_login_options' );

	$output = sprintf('
		<form name="loginform" id="loginform" action="%s" method="post">
			<div class="login-form-container columns">
				<h2 class="title">%s</h2>
				<p>%s</p>
				<div class="simplemodal-login-fields">
					<p>
						<label>%s<br />
							<input type="text" name="log" class="user_login input" value="" size="20" tabindex="10" /></label>
						</p>
						<p>
							<label>%s<br />
								<input type="password" name="pwd" class="user_pass input" value="" size="20" tabindex="20" /></label>
							</p>',
							site_url( 'wp-login.php', 'login_post' ),
							__( 'Return Customers', 'simplemodal-login' ),
							__( 'Sign in now to check out using your saved info.', 'simplemodal-login' ),
							__( 'Username', 'simplemodal-login' ),
							__( 'Password', 'simplemodal-login' )
							);

	ob_start();
	do_action( 'login_form' );
	$output .= ob_get_clean();

	$output .= sprintf('
		<p class="submit">
			<input type="submit" name="wp-submit" value="%s" tabindex="100" />
			<input type="button" class="simplemodal-close" value="%s" tabindex="101" />
			<input type="hidden" name="testcookie" value="1" />
		</p>
		<p class="nav">',
			__( 'Sign in', 'simplemodal-login' ),
			__( 'Cancel', 'simplemodal-login' )
			);

	if ( $users_can_register && $options['registration'] ) {
		$output .= sprintf('<a class="simplemodal-register" href="%s">%s</a>',
			site_url( 'wp-login.php?action=register', 'login' ),
			__( 'Register', 'simplemodal-login' )
			);
	}

	if ( ( $users_can_register && $options['registration'] ) && $options['reset'] ) {
		$output .= ' | ';
	}

	if ( $options['reset'] ) {
		$output .= sprintf('<a class="simplemodal-forgotpw" href="%s" title="%s">%s</a>',
			site_url( 'wp-login.php?action=lostpassword', 'login' ),
			__( 'Password Lost and Found', 'simplemodal-login' ),
			__( 'Lost your password?', 'simplemodal-login' )
			);
	}

	$output .= '
</p>
</div>
<div class="simplemodal-login-activity" style="display:none;"></div>
</div>
<div class="guest-checkout columns">
	<h2>' . __( 'Guests', 'simplemodal-login' ) . '</h2>
	<p>' . __( 'Check out fast and secure.  You can create an account later.', 'simplemodal-login' ) . '</p>
	<a href="/checkout" class="cta final continue-guest">' . __( 'Continue', 'simplemodal-login' ) . '</a>
</div>
</form>';

return $output;
}


/*
 * Remove login from checkout.
 */
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );



/*
 * Add payment logos on checkout page.
 */
add_action( 'storefront_footer', 'gctvshop_add_payment_logos_and_ssl_seal' );
function gctvshop_add_payment_logos_and_ssl_seal() {
	if ( is_checkout() ) {

		echo '<img alt="This Site is Secure" src="/wp-content/uploads/2016/04/ssl-logo4.png">';

		$atts = array(
			'methods' => 'amazon,paypal,visa,mastercard,discover,american-express'
			);
		wc_payment_methods( $atts );
		
	}
}


/*
 * Filter the footer copyright text.
 */
add_filter( 'storefront_copyright_text', 'gctvshop_filter_copyright' );
function gctvshop_filter_copyright( $copyright ) {
	$copyright = str_replace( 'Shop', '', $copyright );
	return $copyright;
}


/*
 * Hide footer bar with CSS if on checkout page.
 */
add_action( 'wp_enqueue_scripts', 'gctvshop_hide_footer_bar_on_checkout' );
function gctvshop_hide_footer_bar_on_checkout() {
	if ( is_checkout() ) {
		$hide_footer = '.sfb-footer-bar { display: none; }';
		wp_add_inline_style( 'storefront-child-style', $hide_footer );
	}	
}



